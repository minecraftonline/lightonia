package com.minecraftonline.lightonia.util;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.*;

import com.minecraftonline.lightonia.commands.CommandHub.BackupType;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.lightonia.ConfigFile;
import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.commands.Import;
import com.minecraftonline.lightonia.commands.Invsee;
import com.minecraftonline.lightonia.commands.Invsee.InventoryType;

public class FileFunctions {

    public static enum FileType {
        DIRECTORY("directory"),
        TAR("tar"),
        COMPRESSED_TAR("compressed tar"),
        BZ2("tar.bz2");

        private final String name;

        private FileType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    // fake uuid for non-player file selections
    private static UUID SERVER_UUID = new UUID(0,0);

    private static HashMap<UUID, String> selectedFiles = new HashMap<UUID, String>();
    private static HashMap<UUID, FileType> selectedFileTypes = new HashMap<UUID, FileType>();
    private static HashMap<UUID, BackupType> selectedBackupTypes = new HashMap<UUID, BackupType>();
    private static HashMap<UUID, String> selectedTarDirectories = new HashMap<UUID, String>();

    public static boolean checkWorldPath(CommandSource src) {
        if (new File(ConfigFile.worldPath).exists()) {
            File[] listOfFiles = new File(ConfigFile.worldPath).listFiles();
            for (File file : listOfFiles) {
                if (file.isDirectory() && (file.getName().equals("region") || file.getName().equals("DIM1") || file.getName().equals("DIM-1"))) {
                    // This worldPath is most likely correct.
                    return true;
                }
            }
            src.sendMessage(Text.of(TextColors.DARK_RED, "The worldPath provided in lightonia.conf does not appear to be correct."));
        } else {
            src.sendMessage(Text.of(TextColors.DARK_RED, "The worldPath provided in lightonia.conf does not exist."));
        }
        return false;
    }

    public static boolean isLightoniaActuallyEmpty() {
        File lightoniaRegionFolder = new File(ConfigFile.worldPath + "/Lightonia/region");
        return !lightoniaRegionFolder.exists() || lightoniaRegionFolder.list().length == 0;
    }

    /**
     * Get the all backups in the given paths
     * @param backupPaths The paths to search
     * @return A list of tuple of backup path -> type
     *             Will return a null list if backup path is not found.
     */
    public static List<Pair<String, List<File>>> getBackupList(List<String> backupPaths) {
        List<Pair<String, List<File>>> backupFiles = new ArrayList<>();
        for (String backupPath : backupPaths) {
            File folder = new File(backupPath);
            File[] listOfFiles = folder.listFiles();
            if (listOfFiles != null) {
                Arrays.sort(listOfFiles, Comparator.comparing(File::getName));
                backupFiles.add(Pair.of(backupPath, Arrays.asList(listOfFiles)));
            }
            else {
                backupFiles.add(Pair.of(backupPath, null));
            }
        }
        return backupFiles;
    }

    public static List<Pair<String, List<File>>> getBackupListForType(BackupType backupType) {
        switch (backupType) {
            case World:
                return getBackupList(ConfigFile.worldBackupPaths);
            case Player:
                return getBackupList(ConfigFile.playerBackupPaths);
            default:
                throw new AssertionError("Missing enum case for " + backupType);
        }
    }

    public static void listContents(MessageReceiver src, BackupType backupType) {
        List<Text> contents = new ArrayList<>();
        List<Pair<String, List<File>>> backupList = getBackupListForType(backupType);
        for (Pair<String, List<File>> pair : backupList) {
            String backupPath = pair.getKey();
            List<File> files = pair.getValue();
            if (files == null) {
                src.sendMessage(Text.of(TextColors.DARK_RED, "The Directory '" + backupPath + "' does not exist."));
                src.sendMessage(Text.of(TextColors.DARK_RED, "This can be changed in lightonia.conf. Make sure to do /Lightonia reload afterwards."));
                continue;
            }

            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (File file : files) {
                try {
                    if (file.isDirectory() || file.getName().endsWith(".tar") || file.getName().endsWith(".tar.bz2")
                            || file.getName().endsWith(".tar.gz") || file.getName().endsWith(".tgz")) {
                        contents.add(Text.builder()
                            .append(Text.of(
                                    TextColors.GOLD, date.format(Files.readAttributes(file.toPath(), BasicFileAttributes.class).creationTime().toMillis()),
                                    TextColors.DARK_RED, "   ", file.getName()))
                            .onClick(TextActions.suggestCommand("/lightonia select " + backupType.name().toLowerCase() + " " + file.getName())).build()
                        );
                    }
                }catch(IOException ioe){
                    //Well then.
                    contents.add(Text.of(TextColors.DARK_RED, file.getName()));
                }
            }
        }
        if (!contents.isEmpty()) {
            PaginationList.builder()
            .contents(contents)
            .linesPerPage(20)
            .padding(Text.of(TextColors.GOLD, "-"))
            .title(Text.of(TextColors.DARK_GREEN, backupType.name().toLowerCase() + " backups"))
            .sendTo(src);
        } else {
            src.sendMessage(Text.of(TextColors.DARK_RED, "No backups found in the specified directories."));
        }
    }

    public static void deleteFile(String fileName) {
        new File(fileName).delete();
    }

    static boolean deleteDirectory(File directory, MessageReceiver source) {
        File[] allContents = directory.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                if (!deleteDirectory(file, source)) {
                    return false;
                }
            }
        }
        try {
            if(!directory.delete()) {
                source.sendMessage(Text.of(TextColors.DARK_GREEN, "Failed to delete the " + directory.getName() + " directory."));
                return false;
            }
        } catch(SecurityException e) {
            source.sendMessage(Text.of(TextColors.DARK_GREEN, "Failed to delete the " + directory.getName() +  " directory due to a permission issue: " + e.getMessage()));
            return false;
        }
        return true;
    }

    static boolean createDataDirSymlink(CommandSource src, String dir) {
        try {
            // Creates the /Lightonia/data directories if they don't already exist
            new File(ConfigFile.worldPath + "/Lightonia/data").mkdirs();
            // Creates the symlinks for advancements and functions
            Files.createSymbolicLink(new File(ConfigFile.worldPath + "/Lightonia/data/" + dir).toPath(), new File(ConfigFile.worldPath + "/data/" + dir).toPath());
            return true;
        } catch (IOException e) {
            try {
                Files.copy(new File(ConfigFile.worldPath + "/data/" + dir).toPath(), new File(ConfigFile.worldPath + "/Lightonia/data/" + dir).toPath(), StandardCopyOption.REPLACE_EXISTING);
                return true;
            } catch (IOException ex) {
                src.sendMessage(Text.of(TextColors.DARK_RED, "Failed to create symbolic link for " + dir + ":\n" + e.getMessage() + "\nAlso failed to copy files as a fallback:\n" + ex.getMessage()));
            }
        }
        return false;
    }

    public static boolean verifyAndSetBackupType(CommandSource src, String playerGivenName, BackupType backupType) {
        List<String> backupPaths = backupType == BackupType.World ? ConfigFile.worldBackupPaths : ConfigFile.playerBackupPaths;
        for (String backupPath : backupPaths) {
            File folder = new File(backupPath + "/" + playerGivenName);
            if (!folder.exists()) {
                continue;
            }

            String absolutePath = folder.getAbsolutePath();
            if (folder.isDirectory()) {
                updateBackupPath(src, backupType, absolutePath, FileType.DIRECTORY);
            } else if (playerGivenName.endsWith(".tar")) {
                updateBackupPath(src, backupType, absolutePath, FileType.TAR);
            } else if (playerGivenName.endsWith(".tar.gz") || playerGivenName.endsWith(".tgz")) {
                updateBackupPath(src, backupType, absolutePath, FileType.COMPRESSED_TAR);
            } else if (playerGivenName.endsWith(".tar.bz2")) {
                updateBackupPath(src, backupType, absolutePath, FileType.BZ2);
            } else {
                continue;
            }
            return true;
        }
        return false;
    }

    private static UUID getSourceUUID(CommandSource src) {
        if (src instanceof Player) {
            return ((Player) src).getUniqueId();
        } else {
            return SERVER_UUID;
        }
    }

    public static void updateBackupPath(CommandSource src, BackupType backupType, String path, FileType fileType) {
        UUID uuid = getSourceUUID(src);
        selectedFiles.put(uuid, path);
        selectedFileTypes.put(uuid, fileType);
        selectedBackupTypes.put(uuid, backupType);
        selectedTarDirectories.remove(uuid);
    }

    public static void setTarDirectory(CommandSource src, String path) {
        selectedTarDirectories.put(getSourceUUID(src), path);
    }

    public static String getBackupPath(CommandSource src) {
        return selectedFiles.get(getSourceUUID(src));
    }

    public static FileType getBackupFileType(CommandSource src) {
        return selectedFileTypes.get(getSourceUUID(src));
    }

    public static BackupType getBackupType(CommandSource src) {
        return selectedBackupTypes.get(getSourceUUID(src));
    }

    public static String getTarDirectory(CommandSource src) {
        return selectedTarDirectories.get(getSourceUUID(src));
    }

    public static boolean validateBackupContentAndSetPath(CommandSource src, BackupType backupType, String directory, String[] acceptedFolders) {
        if (BackupContentValidator.isDirectoryBackup(src)) {
            return BackupContentValidator.validateDirectoryBackup(src, backupType, directory, acceptedFolders);
        } else {
            return BackupContentValidator.validateTarBackup(src, backupType, directory, acceptedFolders);
        }
    }

    public static void transferFromTar(CommandSource src, BackupType backupType, String desiredDirectory, List<String> desiredFiles, String targetName, String dummyUUID, InventoryType inventoryType) {
        String tarFilePath = getBackupPath(src);
        if (tarFilePath == null) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "You have not selected a backup file to use!"));
            return;
        }

        if (backupType != BackupType.World && !(src instanceof Player))
        {
            src.sendMessage(Text.of(TextColors.DARK_RED, "You must be a player to use this command!"));
            return;
        }

        Task.builder().execute(() -> {
            boolean done = TarTransferHelper.processTarFile(src, backupType, tarFilePath, desiredDirectory, desiredFiles, dummyUUID);
            TarTransferHelper.postProcessTransfer(src, done, backupType, targetName, dummyUUID, inventoryType);
        }).async().name("Lightonia - transfer tar file").submit(Lightonia.getPlugin());
    }

    public static boolean transferFiles(CommandSource src, boolean world, String selectedBackup, String backupPath, List<String> files, String type, String dummyUUID) {
        if (world) {
            // Creates parent directories if they do not already exist.
            new File(ConfigFile.worldPath + "/Lightonia/region").mkdirs();
        }

        Set<String> importedFiles = new HashSet<String>();
        boolean allSuccessful = true;
        try {
            for (String file : files) {
                File source = new File(selectedBackup + backupPath + file);
                File destination = new File(ConfigFile.worldPath + (world ? "/Lightonia/region/" + file: "/playerdata/" + dummyUUID + ".dat"));
                if (source.exists()) {
                    Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    importedFiles.add(file);
                } else {
                    Lightonia.getLogger().error(src.getName() + " could not find the " + type + " file at " + source.getAbsolutePath());
                    src.sendMessage(Text.of(TextColors.DARK_RED, "Error: The " + type + " file\n(" + source.getAbsolutePath() + ")\n does not exist."));
                    allSuccessful = false;
                    break;
                }
            }

            if (allSuccessful) {
                if (world) {
                    Import.updateExtentsForImportedRegions(files);
                }
                src.sendMessage(Text.of(TextColors.DARK_GREEN, "Done!"));
            }
        } catch (IOException e) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Copy file error:\n" + e.getMessage()));
            allSuccessful = false;
        } finally {
            if (world) {
                Import.completeImport(src, importedFiles);
            }
        }
        return allSuccessful;
    }
}


class BackupContentValidator {

    static boolean isDirectoryBackup(CommandSource src) {
        return FileFunctions.getBackupFileType(src).equals(FileFunctions.FileType.DIRECTORY);
    }

    static boolean validateDirectoryBackup(CommandSource src, BackupType backupType, String directory, String[] acceptedFolders) {
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (isValidDirectory(file, acceptedFolders)) {
                FileFunctions.updateBackupPath(src, backupType, folder.getAbsolutePath(), FileFunctions.FileType.DIRECTORY);
                return true;
            }
            if (file.isDirectory() && validateDirectoryBackup(src, backupType, file.getAbsolutePath(), acceptedFolders)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isValidDirectory(File file, String[] acceptedFolders) {
        return file.isDirectory() && Arrays.asList(acceptedFolders).contains(file.getName());
    }

    static boolean validateTarBackup(CommandSource src, BackupType backupType, String directory, String[] acceptedFolders) {
        TarArchiveInputStream tarInput = TarTransferHelper.createTarInputStream(src, directory);
        if (tarInput == null) {
            return false;
        }

        try {
            TarArchiveEntry entry;
            while ((entry = tarInput.getNextTarEntry()) != null) {
                if (entry.isDirectory() && isValidTarEntry(entry, backupType, acceptedFolders)) {
                    setTarDirectory(src, entry);
                    return true;
                }
            }
        } catch (IOException e) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Error while reading Tar file: \n" + e.getMessage()));
        } finally {
            try {
                tarInput.close();
            } catch (IOException e) {
                src.sendMessage(Text.of(TextColors.DARK_RED, "Error closing Tar file: \n" + e.getMessage()));
            }
        }
        return false;
    }

    private static boolean isValidTarEntry(TarArchiveEntry entry, BackupType backupType, String[] acceptedFolders) {
        // DIM1 is four characters, and playerdata 10 characters.
        if ((backupType == BackupType.World && entry.getName().length() < 4) || (backupType == BackupType.Player && entry.getName().length() < 10)) {
            return false;
        }

        String[] processedEntry = processTarEntryName(entry);
        String folderName = processedEntry[0];
        String parentFolder = processedEntry[2];

        // For playerdata, ignore the DIM1 and DIM-1 folders if they are parent folders.
        if (backupType == BackupType.Player && (parentFolder.equals("DIM1") || parentFolder.equals("DIM-1"))) {
            return false;
        }

        return Arrays.asList(acceptedFolders).contains(folderName);
    }

    private static void setTarDirectory(CommandSource src, TarArchiveEntry entry) {
        String[] processedEntry = processTarEntryName(entry);
        String parentPath = processedEntry[1];

        FileFunctions.setTarDirectory(src, parentPath);
    }

    private static String[] processTarEntryName(TarArchiveEntry entry) {
        String entryName = entry.getName();
        String entryMinusSlash = entryName.endsWith("/") ? entryName.substring(0, entryName.length() - 1) : entryName;

        String folderName = entryMinusSlash.substring(entryMinusSlash.lastIndexOf("/") + 1);
        String parentPath = entryMinusSlash.substring(0, entryMinusSlash.length() - folderName.length());

        String parentMinusSlash = parentPath.endsWith("/") ? parentPath.substring(0, parentPath.length() - 1) : parentPath;
        String parentFolder = parentMinusSlash.substring(parentMinusSlash.lastIndexOf('/') + 1);

        return new String[]{folderName, parentPath, parentFolder};
    }
}


class TarTransferHelper {

    static TarArchiveInputStream createTarInputStream(CommandSource src, String directory) {
        InputStream fileStream = null;
        BufferedInputStream bufferedStream = null;
        InputStream compressorStream = null;
        FileFunctions.FileType fileType = FileFunctions.getBackupFileType(src);

        if (fileType == null) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "You have not selected a backup file to use!"));
            return null;
        }

        try {
            fileStream = new FileInputStream(directory);
            bufferedStream = new BufferedInputStream(fileStream);

            if (fileType.equals(FileFunctions.FileType.TAR)) {
                return new TarArchiveInputStream(bufferedStream);
            } else {
                if (fileType.equals(FileFunctions.FileType.COMPRESSED_TAR)) {
                    compressorStream = new GzipCompressorInputStream(bufferedStream);
                } else if (fileType.equals(FileFunctions.FileType.BZ2)) {
                    compressorStream = new BZip2CompressorInputStream(bufferedStream);
                }
                return new TarArchiveInputStream(compressorStream);
            }
        } catch (IOException e) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Error creating tar input stream: \n" + e.getMessage()));
            closeQuietly(compressorStream != null ? compressorStream : bufferedStream, src);
            if (compressorStream == null) {
                closeQuietly(fileStream, src);
            }
            return null;
        }
    }

    private static void closeQuietly(Closeable closeable, MessageReceiver src) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                src.sendMessage(Text.of(TextColors.DARK_RED, "Error closing stream: \n" + e.getMessage()));
            }
        }
    }

    static boolean processTarFile(CommandSource src, BackupType backupType, String tarFilePath, String desiredDirectory, List<String> desiredFiles, String dummyUUID) {
        TarArchiveInputStream tarInput = createTarInputStream(src, tarFilePath);
        if (tarInput == null) {
            return false;
        }

        // Ensures temporary directory will be unique.
        long unixTimeNow = System.currentTimeMillis() / 1000L;
        String tempDirName = unixTimeNow + "lightonia_temp_" + backupType.name().toLowerCase();
        File tempDir = new File("config/lightonia", tempDirName);

        Set<String> importedFiles = new HashSet<String>();
        Set<String> filesToTransfer = new HashSet<>(desiredFiles);
        boolean error;
        try {
            tempDir.mkdirs();
            TarArchiveEntry entry;
            while ((entry = tarInput.getNextTarEntry()) != null && !filesToTransfer.isEmpty()) {
                String entryName = entry.getName();
                for (String desiredFile : filesToTransfer) {
                    if (entryName.equals(desiredDirectory + "/" + desiredFile)) {
                        String destination;
                        if (backupType == BackupType.World) {
                            destination = "/Lightonia/region/" + desiredFile;
                        } else {
                            destination = "/playerdata/" + dummyUUID + ".dat";
                        }
                        extractAndCopyFile(src, backupType, tempDir, tarInput, entry, destination);
                        filesToTransfer.remove(desiredFile);
                        importedFiles.add(desiredFile);
                        break;
                    }
                }
            }
            error = false;
        } catch (IOException e) {
            Lightonia.getLogger().error(src.getName() + " encountered an error while reading and copying from Tar file: \n" + e.getMessage());
            src.sendMessage(Text.of(TextColors.DARK_RED, "Error while reading and copying from Tar file: \n" + e.getMessage()));
            error = true;
        } finally {
            try {
                tarInput.close();
            } catch (IOException e) {
                src.sendMessage(Text.of(TextColors.DARK_RED, "Error closing Tar file: \n" + e.getMessage()));
            }
            FileFunctions.deleteDirectory(tempDir, src);
            if (backupType == BackupType.World) {
                Import.completeImport(src, importedFiles);
            }
        }
        String missingFiles = String.join(", ", filesToTransfer);
        if (error) {
            if (!filesToTransfer.isEmpty()) {
                Lightonia.getLogger().warn(src.getName() + " did not import " + backupType.name().toLowerCase() + " files in " + desiredDirectory +
                        ": " + missingFiles + " from " + tarFilePath + " due to an error.");
            }
            return false;
        }
        if (!filesToTransfer.isEmpty()) {
            Lightonia.getLogger().warn(src.getName() + " could not find " + backupType.name().toLowerCase() + " files in " + desiredDirectory +
                    ": " + missingFiles + " from " + tarFilePath + ".");
            src.sendMessage(Text.of(TextColors.DARK_RED, "Error: the following " + backupType.name().toLowerCase() + " files in " + desiredDirectory +
                    " do not exist in the Tar file:\n" + missingFiles + "\nTar file path: " + tarFilePath));
        } else if (backupType == BackupType.World) {
            Import.updateExtentsForImportedRegions(desiredFiles);
        }
        return filesToTransfer.isEmpty();
    }

    private static void extractAndCopyFile(MessageReceiver src, BackupType backupType, File tempDir, TarArchiveInputStream tarInput, TarArchiveEntry entry, String destination) throws IOException {
        // Extract only the filename from the entry's full path
        String entryFileName = new File(entry.getName()).getName();
        File outputFile = new File(tempDir, entryFileName);

        try (OutputStream outputFileStream = new FileOutputStream(outputFile)) {
            IOUtils.copy(tarInput, outputFileStream);
        }

        if (backupType == BackupType.World) {
            new File(ConfigFile.worldPath + "/Lightonia/region").mkdirs();
        }

        File destinationFile = new File(ConfigFile.worldPath + destination);
        Files.copy(outputFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        src.sendMessage(Text.of(TextColors.DARK_GREEN, "...Imported the " + backupType.name().toLowerCase() + " file " + entryFileName + "..."));
    }

    static void postProcessTransfer(CommandSource src, boolean done, BackupType backupType, String targetName, String dummyUUID, InventoryType inventoryType) {
        if (!done) return;

        if (backupType != BackupType.World) {
            Task.builder().execute(() -> Invsee.invseeAfterAsync((Player) src, targetName, dummyUUID, inventoryType))
            .name("Lightonia - open up player inventory").submit(Lightonia.getPlugin());
        } else {
            Task.builder().execute(() -> {
                src.sendMessage(Text.of(TextColors.DARK_GREEN, "\n===Finished importing regions!==="));
                if (WorldFunctions.doesLightoniaExist()) {
                    WorldFunctions.loadLightoniaAndAdjustBorder(src, true);
                }
            }).name("Lightonia - world reload").submit(Lightonia.getPlugin());
        }
    }
}