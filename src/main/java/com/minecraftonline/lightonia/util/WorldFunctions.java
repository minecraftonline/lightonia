package com.minecraftonline.lightonia.util;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import com.minecraftonline.lightonia.ConfigFile;
import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.commands.Teleport;
import com.minecraftonline.lightonia.commands.Import;
import com.minecraftonline.lightonia.commands.Invite;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.WorldArchetype;
import org.spongepowered.api.world.WorldArchetypes;
import org.spongepowered.api.world.difficulty.Difficulties;
import org.spongepowered.api.world.difficulty.Difficulty;
import org.spongepowered.api.world.storage.WorldProperties;

public class WorldFunctions {

    // Used for calculating the size of the world border.
    private static int minX = Integer.MAX_VALUE;
    private static int minZ = Integer.MAX_VALUE;
    private static int maxX = Integer.MIN_VALUE;
    private static int maxZ = Integer.MIN_VALUE;

    public static boolean doesLightoniaExist() {
        Collection<WorldProperties> allWorldProperties = Sponge.getServer().getAllWorldProperties();
        for (WorldProperties properties: allWorldProperties) {
            if (properties.getWorldName().equals("Lightonia")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isLightoniaCreated() {
        Optional<World> theWorld = Sponge.getServer().getWorld("Lightonia");
        return theWorld.isPresent();
    }

    public static boolean isLightoniaLoaded() {
        Optional<World> theWorld = Sponge.getServer().getWorld("Lightonia");
        return theWorld.isPresent() && theWorld.get().isLoaded();
    }

    public static void loadLightoniaAndAdjustBorder(CommandSource src, boolean verbose) {
        if (doesLightoniaExist()) {
            Sponge.getServer().loadWorld("Lightonia");
            adjustWorldBorderForLightonia();
            if (verbose) {
                src.sendMessage(Text.of(TextColors.DARK_GREEN, "Successfully loaded Lightonia."));
            }
        } else if (verbose) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Lightonia does not exist."));
        }
    }

    public static void createLightonia(CommandSource src) {
        if (Import.isLightoniaEmpty()) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Lightonia has no region files. Make sure you use /Lightonia import first."));
            return;
        }
        long unixTimeNow = System.currentTimeMillis() / 1000L;
        if (!doesLightoniaExist()) {
            if (!FileFunctions.createDataDirSymlink(src, "advancements") || !FileFunctions.createDataDirSymlink(src, "functions")) {
                return;
            }
            Difficulty difficulty = Sponge.getServer().getDefaultWorld().map(world -> world.getDifficulty()).orElse(Difficulties.NORMAL);
            String worldName = "Lightonia";
            WorldArchetype worldArchetype = WorldArchetype.builder()
                    .difficulty(difficulty)
                    .from(WorldArchetypes.THE_VOID)
                    .generateSpawnOnLoad(false)
                    .keepsSpawnLoaded(false)
                    .loadsOnStartup(false)
                    .build(worldName + unixTimeNow , worldName);
            try {
                Sponge.getServer().createWorldProperties(worldName, worldArchetype);
            } catch (IOException e) {
                src.sendMessage(Text.of(TextColors.DARK_RED, "An error occurred while creating Lightonia: " + e.getMessage()));
                return;
            }
            loadLightoniaAndAdjustBorder(src, false);
            src.sendMessage(Text.of(TextColors.DARK_GREEN, "Successfully created and loaded Lightonia."));
            return;
        }
        if (!isLightoniaLoaded()) {
            loadLightoniaAndAdjustBorder(src, false);
            src.sendMessage(Text.of(TextColors.DARK_GREEN, "Lightonia already existed, but it has now been loaded."));
            return;
        }
        src.sendMessage(Text.of(TextColors.DARK_RED, "Lightonia already exists and it is already loaded."));
    }

    public static void unloadThenClearLightonia(CommandSource source) {
        source.sendMessage(Text.of(TextColors.DARK_GREEN, "Attempting to clear Lightonia..."));
        if (!doesLightoniaExist()) {
            source.sendMessage(Text.of(TextColors.DARK_RED, "Lightonia does not exist."));
            return;
        }

        Text importingPlayersWithCounts = Import.getImportingPlayersWithCounts(TextColors.DARK_RED);
        if (!importingPlayersWithCounts.toPlain().isEmpty()) {
            source.sendMessage(Text.of(TextColors.DARK_RED, "Failed to clear Lightonia due to ongoing region imports by: ", importingPlayersWithCounts));
            return;
        }

        if (isLightoniaCreated()) {
            World theWorld = Sponge.getServer().getWorld("Lightonia").get();
            if (theWorld.isLoaded()) {
                theWorld.getPlayers().forEach(p -> Teleport.previousLocationTeleport(p));
                if (!Sponge.getServer().unloadWorld(theWorld)) {
                    source.sendMessage(Text.of(TextColors.DARK_RED, "Failed to unload Lightonia for clearing."));
                    return;
                }
            }
        }

        resetExtents();

        if (FileFunctions.isLightoniaActuallyEmpty()) {
            source.sendMessage(Text.of(TextColors.DARK_RED, "Lightonia is already empty."));
            return;
        }

        Lightonia.getLogger().info("Deleting region files and clearing invites from Lightonia");
        // delete region files, but keep the world intact.
        FileFunctions.deleteDirectory(new File(ConfigFile.worldPath + "/Lightonia/region"), source);
        source.sendMessage(Text.of(TextColors.DARK_GREEN, "Successfully cleared Lightonia."));
        Import.clearRegions();
        Invite.clearInvites();
    }

    public static void updateExtents(int regionX, int regionZ) {
        int regionMinX = regionX * 512;
        int regionMaxX = regionMinX + 511;
        int regionMinZ = regionZ * 512;
        int regionMaxZ = regionMinZ + 511;

        minX = Math.min(minX, regionMinX);
        maxX = Math.max(maxX, regionMaxX);
        minZ = Math.min(minZ, regionMinZ);
        maxZ = Math.max(maxZ, regionMaxZ);
    }

    public static void resetExtents() {
        minX = Integer.MAX_VALUE;
        minZ = Integer.MAX_VALUE;
        maxX = Integer.MIN_VALUE;
        maxZ = Integer.MIN_VALUE;
    }

    public static void adjustWorldBorderForLightonia() {
        Optional<World> lightoniaOpt = Sponge.getServer().getWorld("Lightonia");
        if (!lightoniaOpt.isPresent()) {
            return;
        }

        World lightonia = lightoniaOpt.get();

        // Add 1 to centre the border on the block.
        int centerX = (minX + maxX + 1) / 2;
        int centerZ = (minZ + maxZ + 1) / 2;

        // Add 1 to ensure the border encloses the last block.
        int diameterX = maxX - minX + 1;
        int diameterZ = maxZ - minZ + 1;
        int diameter = Math.max(diameterX, diameterZ);

        lightonia.getWorldBorder().setCenter(centerX, centerZ);
        lightonia.getWorldBorder().setDiameter(diameter);
        Lightonia.getLogger().info("Lightonia world border is now centered around X: " + centerX + ", Z: " + centerZ + " with a diameter of " + diameter);
    }

    public static boolean isInsideBorderForLightonia(double x, double z) {
        return x >= minX && x <= maxX && z >= minZ && z <= maxZ;
    }
}