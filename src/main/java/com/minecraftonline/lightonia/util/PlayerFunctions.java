package com.minecraftonline.lightonia.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.InventoryArchetypes;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.lightonia.ConfigFile;
import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.commands.Invsee.InventoryType;

public class PlayerFunctions {

    public static String [] dummyUUIDArray = new String [] {ConfigFile.dummyUUID0,
            ConfigFile.dummyUUID1,
            ConfigFile.dummyUUID2,
            ConfigFile.dummyUUID3,
            ConfigFile.dummyUUID4,
            ConfigFile.dummyUUID5,
            ConfigFile.dummyUUID6,
            ConfigFile.dummyUUID7};

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            Lightonia.getLogger().error("Failed to load MySQL JDBC driver: " + e.getMessage());
        }
    }

    public static String getUUID(Player player, String name) throws IOException {
        name = name.replaceAll("[^A-Za-z0-9_]", "");
        String urlString = "https://api.mojang.com/users/profiles/minecraft/" + name;
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        int status = con.getResponseCode();
        if (status == HttpURLConnection.HTTP_NO_CONTENT) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "Failed to find player's UUID."));
            return "";
        }
        if (status != HttpURLConnection.HTTP_OK) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "Failed to connect to Mojang's API."));
            return "";
        }
        try (InputStream is = con.getInputStream()) {
            Scanner scanner = new Scanner(is);
            String response = scanner.useDelimiter("\\A").next();
            scanner.close();
            String stringUUID = "";
            Pattern pattern = Pattern.compile("\"?id\"?\\s*:\\s*\"?([a-zA-Z0-9-]+)\"?\\s*,?");
            Matcher matcher = pattern.matcher(response);
            if (matcher.find()) {
                stringUUID = matcher.group(1);
            }
            stringUUID = stringUUID.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5");
            return stringUUID;
        } catch (IOException e) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "Failed to read response from Mojang's API: " + e));
            return "";
        } finally {
            con.disconnect();
        }
    }

    public static String getUUIDFromDatabase(Player player, String name) {
        String uuid = "";
        name = name.replaceAll("[^A-Za-z0-9_]", "");

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + ConfigFile.dbAddress + "/" + ConfigFile.dbName, ConfigFile.dbUsername, ConfigFile.dbPassword);
            statement = connection.prepareStatement(ConfigFile.dbQuery);
            statement.setString(1, name);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                uuid = resultSet.getString("uuid");
            }
        } catch (SQLException e) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "Error connecting to the database: " + e.getMessage()));
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    player.sendMessage(Text.of(TextColors.DARK_RED, "Error closing resultSet " + e.getMessage()));
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    player.sendMessage(Text.of(TextColors.DARK_RED, "Error closing statement " + e.getMessage()));
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    player.sendMessage(Text.of(TextColors.DARK_RED, "Error closing connection " + e.getMessage()));
                }
            }
        }

        // Fallback to Mojang's API if UUID is empty
        if (uuid.isEmpty()) {
            try {
                uuid = getUUID(player, name);
            } catch (IOException ex) {
                player.sendMessage(Text.of(TextColors.DARK_RED, "Fallback to Mojang's API failed: " + ex.getMessage()));
            }
        }
        return uuid;
    }

    public static Optional<User> getUser(String stringUUID) {
        UUID uuid = UUID.fromString(stringUUID);
        Optional<UserStorageService> userStorage = Sponge.getServiceManager().provide(UserStorageService.class);
        return userStorage.get().get(uuid);
    }

    public static void seeInventory(Player player, User user, String targetName, InventoryType inventoryType) {
        Inventory build = Inventory.builder()
                .of(InventoryArchetypes.DOUBLE_CHEST)
                .build(Lightonia.getPlugin());

        Iterable<Inventory> slots;

        switch(inventoryType) {
            case INVENTORY:
                slots = user.getInventory().slots();
                break;
            case ENDERCHEST:
                slots = user.getEnderChestInventory().slots();
                break;
            default:
                Lightonia.getLogger().error(player.getName() + " tried to open an unknown inventory type of \"" + inventoryType.getName() + "\" for player: " + targetName);
                player.sendMessage(Text.of(TextColors.DARK_RED, "Unknown inventory type: ", inventoryType.getName()));
                return;
        }

        for(Inventory slot : slots) {
            if (slot.peek().isPresent()) {
                build.offer(slot.peek().get());
            }
        }
        Lightonia.getLogger().info(player.getName() + " opened backup " + inventoryType.getName() + " of " + targetName + ".");
        player.openInventory(build.parent(), Text.of(TextColors.DARK_RED, targetName + "'s " + inventoryType.getName()));
    }

    public static void shiftDummyArray() {
        int size = PlayerFunctions.dummyUUIDArray.length;
        String originalLastElement = PlayerFunctions.dummyUUIDArray[size-1];
        for(int i=size-1; i>0; i--) {
            PlayerFunctions.dummyUUIDArray[i] = PlayerFunctions.dummyUUIDArray[i-1];
        }
        PlayerFunctions.dummyUUIDArray[0] = originalLastElement;
    }
}