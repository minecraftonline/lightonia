package com.minecraftonline.lightonia.commands;

import java.util.Optional;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.storage.WorldProperties;

import com.minecraftonline.lightonia.Lightonia;

public class ImportFrom extends Import {

    private static final Text COORDS = Text.of("coordinates");
    private static final Text WORLD = Text.of("world");

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        WorldProperties worldProperties = args.requireOne(WORLD);
        Optional<World> world = Sponge.getServer().getWorld(worldProperties.getWorldName());
        if (!world.isPresent()) {
            throw new CommandException(Text.of(TextColors.DARK_RED, "World \"", worldProperties.getWorldName(), "\" is not currently loaded!"));
        }
        importRegions(src, args.requireOne(COORDS), world.get(), true);
        return CommandResult.success();
    }

    public static CommandCallable getCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "importfrom.use")
                .executor(new ImportFrom())
                .arguments(GenericArguments.seq(
                        GenericArguments.vector3d(COORDS),
                        GenericArguments.world(WORLD)))
                .build();
    }
}
