package com.minecraftonline.lightonia.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.util.FileFunctions;

public class List implements CommandExecutor {

    private static final Text BACKUP_TYPE = Text.of("backup_type");

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) {
        CommandHub.BackupType backupType = args.requireOne(BACKUP_TYPE);
        // note this uses the select permission rather than the list one
        // as there is no point listing what can not be selected
        if (!src.hasPermission(Lightonia.BASE_PERMISSION + "select." + backupType.name().toLowerCase())) {
            src.sendMessage(Text.of("You do not have permission to list ", backupType.name().toLowerCase(), " backups."));
            return CommandResult.success();
        }
        FileFunctions.listContents(src, backupType);
        return CommandResult.success();
    }

    public static CommandCallable getCommand() {
        return CommandSpec.builder()
                .arguments(GenericArguments.enumValue(BACKUP_TYPE, CommandHub.BackupType.class))
                .permission(Lightonia.BASE_PERMISSION + "list.use")
                .executor(new List())
                .build();
    }
}