package com.minecraftonline.lightonia.commands;

import java.util.Optional;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.chat.ChatTypes;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.world.World;

import com.minecraftonline.lightonia.ConfigFile;
import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.util.WorldFunctions;

public class CommandHub {

    public enum BackupType {
        World,
        Player
    }

    private static final Text MESSAGE = Text.of("message");

    public static CommandCallable getRootCommand() {
        CommandCallable createSpec = CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "create.use")
                .executor((src, args) -> {
                    WorldFunctions.createLightonia(src);
                    return CommandResult.success();
                })
                .build();

        CommandCallable clearSpec = CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "clear.use")
                .executor((src, args) -> {
                    WorldFunctions.unloadThenClearLightonia(src);
                    return CommandResult.success();
                })
                .build();

        CommandCallable reloadSpec = CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "reload.use")
                .executor((src, args) -> {
                    src.sendMessage(Text.of(TextColors.DARK_GREEN, "Reloading config file."));
                    ConfigFile.loadConfig();
                    return CommandResult.success();
                })
                .build();

        CommandCallable announceSpec = CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "announce.use")
                .arguments(GenericArguments.text(MESSAGE, TextSerializers.JSON, true))
                .executor((src, args) -> {
                    Optional<World> world = Sponge.getServer().getWorld("Lightonia");
                    if (world.isPresent()) {
                        if (world.get().getPlayers().isEmpty()) {
                            throw new CommandException(Text.of("There are no players currently in Lightonia."));
                        }
                        Text message = args.requireOne(MESSAGE);
                        world.get().sendMessage(ChatTypes.ACTION_BAR, message);
                        world.get().sendMessage(ChatTypes.SYSTEM, message);
                    } else {
                        throw new CommandException(Text.of("Lightonia is not currently available."));
                    }
                    src.sendMessage(Text.of(TextColors.DARK_GREEN, "Sent message to Lightonia."));
                    return CommandResult.success();
                })
                .build();

        return CommandSpec.builder()
                .description(Text.of("Lightonia commands"))
                .child(Select.getCommand(), "select")
                .child(Invsee.getInvseeCommand(), "invsee")
                .child(Invsee.getEcseeCommand(), "endersee", "ecsee")
                .child(List.getCommand(), "list")
                .child(Help.getCommand(), "help", "?")
                .child(Import.getCommand(), "import")
                .child(ImportFrom.getCommand(), "importfrom")
                .child(Query.getCommand(), "query", "info", "q")
                .child(createSpec, "create")
                .child(Teleport.getTpCommand(), "tp")
                .child(Teleport.getReturnCommand(), "return", "r")
                .child(Teleport.getSwitchCommand(), "switch", "s")
                .child(Invite.getInviteCommand(), "invite")
                .child(Invite.getAcceptCommand(), "accept")
                .child(clearSpec, "clear")
                .child(reloadSpec, "reload")
                .child(announceSpec, "announce")
                .build();
    }
}