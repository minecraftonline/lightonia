package com.minecraftonline.lightonia.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.*;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.util.FileFunctions;
import com.minecraftonline.lightonia.util.FileFunctions.FileType;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Select implements CommandExecutor {

    private static final Text BACKUP_TYPE = Text.of("backup_type");
    private static final Text BACKUP = Text.of("backup");

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        CommandHub.BackupType backupType = args.requireOne(BACKUP_TYPE);
        if (!src.hasPermission(Lightonia.BASE_PERMISSION + "select." + backupType.name().toLowerCase())) {
            throw new CommandException(Text.of("You do not have permission to select ", backupType.name().toLowerCase(), " backups."));
        }
        if (Import.isImporting(src)) {
            throw new CommandException(Text.of("You can not select a backup while importing regions from another backup."));
        }
        if (Invsee.isImporting(src)) {
            throw new CommandException(Text.of("You can not select a backup while opening an inventory from another backup."));
        }

        String backup = args.requireOne(BACKUP);

        if (!FileFunctions.verifyAndSetBackupType(src, backup, backupType)) {
            Lightonia.getLogger().info(src.getName() + " tried to select " + backupType.name().toLowerCase() + " backup: \"" + backup + "\" but it was not found.");
            throw new CommandException(Text.of(TextColors.DARK_RED, "Invalid " + backupType.name().toLowerCase() + " backup. Choose one from /Lightonia list " + backupType.name().toLowerCase() + "s and check your spelling."));
        }
        String[] searchFolders = getBackupSearchPathsForType(backupType);
        String selectedBackup = FileFunctions.getBackupPath(src);
        if (FileFunctions.validateBackupContentAndSetPath(src, backupType, selectedBackup, searchFolders)) {
            reportSuccess(src, selectedBackup, backupType);
            return CommandResult.success();
        } else {
            reportFailure(src, selectedBackup, backupType, searchFolders);
            throw new CommandException(Text.of("Failed to select a backup"));
        }
    }

    private static String[] getBackupSearchPathsForType(CommandHub.BackupType backupType) {
        switch (backupType) {
            case World: return new String[]{"region", "DIM1", "DIM-1"};
            case Player: return new String[]{"playerdata"};
            default: throw new AssertionError("Missing case in enum");
        }
    }

    private static void reportSuccess(CommandSource src, String backupName, CommandHub.BackupType backupType) {
        FileType fileType = FileFunctions.getBackupFileType(src);

        String successMessage;
        if ("directory".equals(fileType.getName())) {
            successMessage = "Successfully found " + backupType.name().toLowerCase() + " backup:\n" + backupName;
        } else {
            successMessage = "Successfully found " + backupType.name().toLowerCase() + " backup inside the " + fileType.getName() + " file\n" + backupName + ": " + FileFunctions.getTarDirectory(src);
        }

        Lightonia.getLogger().info(src.getName() + " selected " + backupType.name().toLowerCase() + " backup: \"" + backupName + "\".");
        src.sendMessage(Text.of(TextColors.DARK_GREEN, successMessage));
    }

    private static void reportFailure(CommandSource src, String backupName, CommandHub.BackupType backupType, String[] searchFolders) {
        FileType fileType = FileFunctions.getBackupFileType(src);
        String folderType = searchFolders[0];

        String folderDescription = "any " + folderType + " folders";
        if (backupType == CommandHub.BackupType.Player && "playerdata".equals(folderType)) {
            folderDescription = "a playerdata folder";
        }

        String locationDescription = "directory".equals(fileType.getName()) ? " within the directory " : " within the " + fileType.getName() + " file ";
        Lightonia.getLogger().info(src.getName() + " tried to select " + backupType.name().toLowerCase() + " backup: \"" + backupName + "\" but it failed.");
        src.sendMessage(Text.of(TextColors.DARK_RED, "Failed to find " + folderDescription + locationDescription + backupName));
    }

    public static class BackupArgument extends CommandElement {

        protected BackupArgument(@Nullable Text key) {
            super(key);
        }

        @Nullable
        @Override
        protected Object parseValue(CommandSource source, CommandArgs args) throws ArgumentParseException {
            return args.next();
        }

        @Override
        public List<String> complete(CommandSource src, CommandArgs args, CommandContext context) {
            String prefix = args.nextIfPresent().orElse("");
            return context.<CommandHub.BackupType>getOne(BACKUP_TYPE).map(backupType -> FileFunctions.getBackupListForType(backupType).stream()
                    .filter(p -> p.getValue() != null)
                    .flatMap(p -> p.getValue().stream())
                    .map(File::getName)
                    .filter(s -> s.startsWith(prefix))
                    .collect(Collectors.toList())).orElse(Collections.emptyList());
        }
    }

    public static CommandCallable getCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "select.use")
                .executor(new Select())
                .arguments(GenericArguments.seq(
                        GenericArguments.enumValue(BACKUP_TYPE, CommandHub.BackupType.class)),
                        new BackupArgument(BACKUP))
                .build();
    }
}
