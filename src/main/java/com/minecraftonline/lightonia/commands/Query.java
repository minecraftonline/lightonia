package com.minecraftonline.lightonia.commands;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.util.WorldFunctions;

public class Query implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        // regions
        Text.Builder textBuilder = Text.builder();
        Map<String, String> regions = Import.getRegions();
        if (regions.isEmpty()) {
            textBuilder.append(Text.of(TextColors.DARK_RED, "Lightonia is empty."));
        } else {
            boolean first = true;
            textBuilder.append(Text.of(TextColors.GREEN, "Lightonia contains ", regions.size(), " region", regions.size() != 1 ? "s" : "", ": "));
            for (Map.Entry<String, String> regionEntry : regions.entrySet()) {
                if (!first) {
                    textBuilder.append(Text.of(TextColors.GREEN, ", "));
                }
                first = false;

                textBuilder.append(Text.builder()
                        .append(Text.of(TextColors.DARK_GRAY, regionEntry.getKey()))
                        .onHover(TextActions.showText(Text.of(TextColors.DARK_RED, regionEntry.getValue()))).build()
                );
            }
        }
        // players
        textBuilder.append(Text.NEW_LINE);
        Collection<Player> players = Collections.emptyList();
        if (WorldFunctions.isLightoniaCreated()) {
            World theWorld = Sponge.getServer().getWorld("Lightonia").get();
            if (theWorld.isLoaded()) players = theWorld.getPlayers();
        }
        if (players.isEmpty()) {
            textBuilder.append(Text.of(TextColors.GREEN, "With no players currently in it."));
        } else {
            textBuilder.append(Text.of(
                TextColors.GREEN, "With ", players.size(), " player", (players.size() == 1 ? "" : "s"), " in it: ",
                TextColors.WHITE, players.stream().map(player -> player.getName()).collect(Collectors.joining(", ")),
                TextColors.GREEN, "."
            ));
        }
        // imports
        textBuilder.append(Text.NEW_LINE);
        Text importingPlayersWithCounts = Import.getImportingPlayersWithCounts(TextColors.DARK_RED);
        if (importingPlayersWithCounts.toPlain().isEmpty()) {
            textBuilder.append(Text.of(TextColors.GREEN, "And has no ongoing region imports."));
        } else {
            textBuilder.append(Text.of(TextColors.DARK_RED, "And has ongoing region imports by: ", importingPlayersWithCounts));
        }
        src.sendMessage(textBuilder.build());
        return CommandResult.success();
    }

    public static CommandCallable getCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "query.use")
                .executor(new Query())
                .build();
    }
}