package com.minecraftonline.lightonia.commands;

import java.util.Optional;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.lightonia.ConfigFile;
import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.commands.CommandHub.BackupType;
import com.minecraftonline.lightonia.util.FileFunctions;
import com.minecraftonline.lightonia.util.PlayerFunctions;
import com.minecraftonline.lightonia.util.FileFunctions.FileType;

public class Invsee implements CommandExecutor {

    public static enum InventoryType {
        NONE("none"), // world backup instead
        INVENTORY("inventory"),
        ENDERCHEST("ender chest");

        private final String name;

        private InventoryType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    // map of players with what inventory they are currently importing
    private static Map<String, String> importers = new HashMap<String, String>();

    private static final Text USER_KEY = Text.of("user");
    private static final Text UUID_KEY = Text.of("uuid");

    private InventoryType inventoryType;

    public Invsee(InventoryType inventoryType) {
        this.inventoryType = inventoryType;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("You must be a player to execute this command!"));
        }
        if (importers.containsKey(src.getName())) {
            throw new CommandException(Text.of(TextColors.DARK_RED, "You are already opening an inventory (",
                    importers.get(src.getName()), ").", Text.NEW_LINE, "Please wait for that to complete first."));
        }
        Player player = (Player) src;
        if (FileFunctions.getBackupPath(src) == null) {
            throw new CommandException(Text.of(TextColors.DARK_RED, "Please select a player backup with /Lightonia select player <backup>. Choose one from /Lightonia list player."));
        }
        if (!FileFunctions.getBackupType(src).equals(BackupType.Player)) {
            throw new CommandException(Text.of(TextColors.DARK_RED, "You have selected a non-player backup, select a player backup with /Lightonia select player <backup>."));
        }
        final String targetName;
        final UUID targetUUID;
        if (args.hasAny(UUID_KEY)) {
            targetUUID = args.requireOne(UUID_KEY);
            targetName = targetUUID.toString();
        }
        else {
            User user = args.requireOne(USER_KEY);
            targetUUID = user.getUniqueId();
            targetName = user.getName();
        }
        importers.put(player.getName(), targetName + "'s " + inventoryType.getName());

        src.sendMessage(Text.of(TextColors.GRAY, "Retrieving .dat file for " + targetName));
        String dummyUUID = PlayerFunctions.dummyUUIDArray[0];
        List<String> playerDataFiles = Collections.singletonList(targetUUID + ".dat");
        FileType fileType = FileFunctions.getBackupFileType(src);
        if (fileType.equals(FileType.TAR) || fileType.equals(FileType.COMPRESSED_TAR) || fileType.equals(FileType.BZ2)) {
            FileFunctions.transferFromTar(player, CommandHub.BackupType.Player, FileFunctions.getTarDirectory(src) + "playerdata",
                    playerDataFiles, targetName, dummyUUID, this.inventoryType);
        } else {
            if (FileFunctions.transferFiles(player, false, FileFunctions.getBackupPath(src), "/playerdata/", playerDataFiles, "player.dat", dummyUUID)) {
                invseeAfterAsync(player, targetName, dummyUUID, this.inventoryType);
            }
        }
        return CommandResult.success();
    }

    public static void invseeAfterAsync(Player player, String targetName, String dummyUUID, InventoryType inventoryType) {
        Optional<User> dummyUser = PlayerFunctions.getUser(dummyUUID);
        if (dummyUser.isPresent()) {
            player.sendMessage(Text.of(TextColors.DARK_GREEN, "Opening up the " + inventoryType.getName() + " of " + targetName + "..."));
            PlayerFunctions.seeInventory(player, dummyUser.get(), targetName, inventoryType);
            FileFunctions.deleteFile(ConfigFile.worldPath + "/playerdata/" + dummyUUID + ".dat");
        } else {
            player.sendMessage(Text.of(TextColors.DARK_RED, "Failed to get dummy user matching the UUID " + dummyUUID));
        }
        importers.remove(player.getName());
    }

    public static CommandCallable getInvseeCommand() {
        return CommandSpec.builder()
                .arguments(GenericArguments.onlyOne(GenericArguments.firstParsing(
                        GenericArguments.user(USER_KEY),
                        GenericArguments.uuid(UUID_KEY))))
                .permission(Lightonia.BASE_PERMISSION + "invsee.use")
                .executor(new Invsee(InventoryType.INVENTORY))
                .build();
    }

    public static CommandCallable getEcseeCommand() {
        return CommandSpec.builder()
                .arguments(GenericArguments.onlyOne(GenericArguments.firstParsing(
                        GenericArguments.user(USER_KEY),
                        GenericArguments.uuid(UUID_KEY))))
                .permission(Lightonia.BASE_PERMISSION + "ecsee.use")
                .executor(new Invsee(InventoryType.ENDERCHEST))
                .build();
    }

    public static boolean isImporting(CommandSource src) {
        return importers.containsKey(src.getName());
    }
}