package com.minecraftonline.lightonia.commands;

import java.util.HashMap;
import java.util.Optional;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.util.WorldFunctions;

public class Teleport implements CommandExecutor {

    public enum TeleportType {
        Lightonia,
        Return,
        Switch
    }

    private static final HashMap<String, Object[]> positionBeforeTeleport = new HashMap<String, Object[]>();

    private final TeleportType teleportType;

    public Teleport(TeleportType teleportType) {
        this.teleportType = teleportType;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("You must be a player to use this command!"));
        }
        Player player = (Player) src;
        switch(teleportType) {
        case Return:
            Teleport.previousLocationTeleport(player);
            break;
        case Lightonia:
            Teleport.lightoniaTeleport(player, player.getPosition(), player.getPosition(), true);
            break;
        case Switch:
            Teleport.switchTeleport(player);
            break;
        }
        return CommandResult.success();
    }

    protected static boolean lightoniaTeleport(Player player, Vector3d originalPosition, Vector3d newPosition, boolean safe) {
        String originalWorld = player.getWorld().getName();
        if (originalWorld.equals("Lightonia")) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "You are already in Lightonia!"));
            return false;
        }

        if (WorldFunctions.doesLightoniaExist()) {
            Text importingPlayersWithCounts = Import.getImportingPlayersWithCounts(TextColors.DARK_RED);
            if (!importingPlayersWithCounts.toPlain().isEmpty()) {
                player.sendMessage(Text.of(TextColors.DARK_RED, "Teleportation to Lightonia is currently unavailable due to ongoing region imports by: ", importingPlayersWithCounts));
                return false;
            }

            if (WorldFunctions.isLightoniaCreated()) {
                if (!WorldFunctions.isInsideBorderForLightonia(newPosition.getX(), newPosition.getZ())) {
                    player.sendMessage(Text.of(TextColors.DARK_RED, "This location is not currently present in Lightonia."));
                    return false;
                }
                if (safe) {
                    Optional<Location<World>> safeLocation = Sponge.getGame().getTeleportHelper().getSafeLocation(
                            new Location<World>(Sponge.getServer().getWorld("Lightonia").get(), newPosition));
                    if (!safeLocation.isPresent()) {
                        Lightonia.getLogger().warn("Did not teleport " + player.getName() + " to Lightonia due to unsafe destination");
                        player.sendMessage(Text.of(TextColors.DARK_RED, "No safe location was found for your current position in Lightonia, aborting teleport."));
                        return false;
                    }
                    newPosition = safeLocation.get().getPosition();
                }
                player.sendMessage(Text.of(TextColors.DARK_GRAY, "Teleporting to Lightonia..."));
                player.setVehicle(null);
                if (player.transferToWorld("Lightonia", newPosition)) {
                    positionBeforeTeleport.put(player.getName(), new Object[] {originalWorld, originalPosition});
                    Lightonia.getLogger().info(player.getName() + " teleported to Lightonia.");
                    return true;
                } else {
                    Lightonia.getLogger().warn("Could not teleport " + player.getName() + " to Lightonia");
                    return false;
                }
            } else {
                player.sendMessage(Text.of(TextColors.DARK_RED, "The world Lightonia is not created! Try /Lightonia create."));
                return false;
            }
        } else {
            player.sendMessage(Text.of(TextColors.DARK_RED, "The world Lightonia does not exist! Try /Lightonia create and also check out /Lightonia help."));
            return false;
        }
    }

    public static void previousLocationTeleport(Player player) {
        if (!player.getWorld().getName().equals("Lightonia")) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "You are not in Lightonia!"));
            return;
        }
        if (positionBeforeTeleport.containsKey(player.getName())) {
            player.sendMessage(Text.of(TextColors.DARK_GRAY, "Teleporting to original position..."));
            player.setVehicle(null);
            String world = (String) positionBeforeTeleport.get(player.getName())[0];
            if (player.transferToWorld(world, (Vector3d) positionBeforeTeleport.get(player.getName())[1])) {
                Lightonia.getLogger().info(player.getName() + " teleported out of Lightonia to previous location in world: " + world);
                positionBeforeTeleport.remove(player.getName());
                return;
            } else {
                Lightonia.getLogger().warn("Could not teleport " + player.getName() + " from Lightonia to to world: " + world);
                player.sendMessage(Text.of(TextColors.DARK_RED, "Could not return you to your previous world, trying the default world..."));
            }
        } else {
            player.sendMessage(Text.of(TextColors.DARK_GRAY, "It appears you did not use /Lightonia tp. Teleporting you to the default world..."));
        }
        defaultWorldTeleport(player);
        positionBeforeTeleport.remove(player.getName());
    }

    protected static void defaultWorldTeleport(Player player) {
        Optional<World> world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName());
        if (world.isPresent()) {
            // send them to the default world spawn
            Lightonia.getLogger().info(player.getName() + " teleported out of Lightonia to the default world (" + world.get().getName() + ") spawn.");
            player.setVehicle(null);
            player.setLocation(world.get().getSpawnLocation());
        } else {
            Lightonia.getLogger().warn("Could not find the default world (" + Sponge.getServer().getDefaultWorldName() + " to teleport " + player.getName() + " to from Lightonia.");
            player.sendMessage(Text.of(TextColors.DARK_RED, "Could not find the default world to teleport to!"));
        }
    }

    protected static void switchTeleport(Player player) {
        if (!player.getWorld().getName().equals("Lightonia")) {
            Teleport.lightoniaTeleport(player, player.getPosition(), player.getPosition(), false);
            return;
        }
        String world;
        if (positionBeforeTeleport.containsKey(player.getName())) {
            // world they were in previously
            world = (String) positionBeforeTeleport.get(player.getName())[0];
        } else {
            // default world
            world = Sponge.getServer().getDefaultWorldName();
        }
        player.sendMessage(Text.of(TextColors.DARK_GRAY, "Teleporting to ", world, "..."));
        player.setVehicle(null);
        if (player.transferToWorld(world, player.getPosition())) {
            Lightonia.getLogger().info(player.getName() + " teleported to out of Lightonia to the same location in world: " + world);
        } else {
            Lightonia.getLogger().warn("Could not teleport " + player.getName() + " from Lightonia to to world: " + world);
            player.sendMessage(Text.of(TextColors.DARK_RED, "Could not return you to your previous world, trying the default world..."));
            defaultWorldTeleport(player);
        }
        positionBeforeTeleport.remove(player.getName());
    }

    public static CommandCallable getTpCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "tp.use")
                .executor(new Teleport(TeleportType.Lightonia))
                .build();
    }

    public static CommandCallable getReturnCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "return.use")
                .executor(new Teleport(TeleportType.Return))
                .build();
    }

    public static CommandCallable getSwitchCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "switch.use")
                .executor(new Teleport(TeleportType.Switch))
                .build();
    }
}