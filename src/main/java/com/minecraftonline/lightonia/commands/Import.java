package com.minecraftonline.lightonia.commands;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.DimensionType;
import org.spongepowered.api.world.DimensionTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.lightonia.Lightonia;
import com.minecraftonline.lightonia.commands.CommandHub.BackupType;
import com.minecraftonline.lightonia.commands.Invsee.InventoryType;
import com.minecraftonline.lightonia.util.FileFunctions;
import com.minecraftonline.lightonia.util.FileFunctions.FileType;
import com.minecraftonline.lightonia.util.WorldFunctions;

public class Import implements CommandExecutor {

    // A map to keep track of the number of ongoing imports for each player
    private static final ConcurrentHashMap<String, Pair<AtomicInteger, List<String>>> ongoingImports = new ConcurrentHashMap<>();

    // regions that have been imported, with the backup filename they were from
    private static Map<String, String> importedRegions = new HashMap<String, String>();

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of(TextColors.DARK_RED, "You must be a player to use this command!"));
        }
        Player player = (Player) src;
        importRegions(player, player.getPosition(), player.getWorld(), false);
        return CommandResult.success();
    }

    static void importRegions(CommandSource src, Vector3d coordinates, World world, boolean large) {
        if (!FileFunctions.checkWorldPath(src)) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Please change the worldPath in lightonia.conf to the path that has 'region/DIM1/DIM-1' in it."));
            return;
        };

        if (FileFunctions.getBackupPath(src) == null) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Please select a world backup with /Lightonia select world <backup>. Choose one from /Lightonia list world."));
            return;
        }

        if (!FileFunctions.getBackupType(src).equals(BackupType.World)) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "You have selected a non-world backup, select a world backup with /Lightonia select world <backup>."));
            return;
        }

        if (world.getName().equals("Lightonia")) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "You cannot import a region from Lightonia!"));
            return;
        }

        if (WorldFunctions.doesLightoniaExist() && WorldFunctions.isLightoniaLoaded()) {
            World lightoniaWorld = Sponge.getServer().getWorld("Lightonia").get();
            if (!Sponge.getServer().unloadWorld(lightoniaWorld)) {
                src.sendMessage(Text.of(TextColors.DARK_RED, "Cannot import region file as someone is inside Lightonia!"));
                return;
            }
        }

        DimensionType dimension = world.getDimension().getType();
        String folder = "";

        if (dimension.equals(DimensionTypes.OVERWORLD)) {
            folder = "";
        } else if (dimension.equals(DimensionTypes.NETHER)) {
            folder = "DIM-1/";
        } else if (dimension.equals(DimensionTypes.THE_END)) {
            folder = "DIM1/";
        }
        else {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Failed to identify the dimension: " + dimension.getName()));
            return;
        }

        Text.Builder messageBuilder = Text.builder();

        Location<World> location = new Location<World>(world, coordinates);
        List<String> regionsToImport;
        if (large) {
            // import all the neighbouring regions to make a 3x3 area with a nice square world border
            // (assuming no other regions are currently imported)
            // as world borders can only be square, not a rectangle.
            // This also acts as a way to make each import "fair", always providing the same
            // amount of regions no matter what your position is.
            // And not having a world border right next to the player.
            regionsToImport = getRegionsAround(messageBuilder, location.getChunkPosition(), 1);
        } else {
            regionsToImport = getNearbyRegions(messageBuilder, location.getChunkPosition(), 5);
        }

        // check if they are already importing this area
        if (ongoingImports.containsKey(src.getName()) && ongoingImports.get(src.getName()).getRight().stream().anyMatch(regionsToImport::contains))
        {
            src.sendMessage(Text.of(TextColors.DARK_RED, "You are already importing regions that would overlap with this import."));
            return;
        }
        // check if someone else is already importing this area
        String overlappingImporters = ongoingImports.entrySet().stream()
            .filter(entry -> entry.getKey() != src.getName() && entry.getValue().getRight().stream().anyMatch(regionsToImport::contains))
            .map(entry -> entry.getKey())
            .collect(Collectors.joining(", "));
        if (!overlappingImporters.isEmpty()) {
            src.sendMessage(Text.of(TextColors.DARK_RED, "Others are currently importing regions that would overlap with this import: ",
                                    TextColors.YELLOW, overlappingImporters));
            return;
        }

        // don't worry if someone has already imported this area, it can be overridden

        src.sendMessage(messageBuilder.build());

        Lightonia.getLogger().info(src.getName() + " is importing regions: " + String.join(", ", regionsToImport) + " from " + dimension.getName() + " into Lightonia.");
        // Increment ongoing import count for the player using the import command.
        ongoingImports.compute(src.getName(), (key, pair) -> {
            if (pair == null) {
                return Pair.of(new AtomicInteger(1), regionsToImport);
            } else {
                pair.getLeft().incrementAndGet();
                pair.getRight().addAll(regionsToImport);
                return pair;
            }
        });

        FileType fileType = FileFunctions.getBackupFileType(src);
        if (fileType.equals(FileType.TAR) || fileType.equals(FileType.COMPRESSED_TAR) || fileType.equals(FileType.BZ2)) {
            FileFunctions.transferFromTar(src, CommandHub.BackupType.World, FileFunctions.getTarDirectory(src) + folder + "region", regionsToImport, "", "", InventoryType.NONE);
        } else {
            if (FileFunctions.transferFiles(src, true, FileFunctions.getBackupPath(src), "/" + folder + "region/", regionsToImport, "region", "")) {
                if (WorldFunctions.doesLightoniaExist()) {
                    src.sendMessage(Text.of(TextColors.DARK_GREEN, "Reloading Lightonia..."));
                    WorldFunctions.loadLightoniaAndAdjustBorder(src, true);
                }
            }
        }
    }

    private static List<String> getRegionsAround(Text.Builder messageBuilder, Vector3i chunkCoords, int radius) {
        int regionX = chunkCoords.getX() >> 5;
        int regionZ = chunkCoords.getZ() >> 5;

        List<String> regions = new ArrayList<>();
        for (int x = -radius; x <= radius; x++) {
            for (int z = -radius; z <= radius; z++) {
                regions.add("r." + (regionX + x) + "." + (regionZ + z) + ".mca");
            }
        }
        messageBuilder.append(Text.of(TextColors.GRAY, "Importing regions: ", TextColors.DARK_GRAY, String.join(", ", regions)));
        return regions;
    }

    private static List<String> getNearbyRegions(Text.Builder messageBuilder, Vector3i chunkCoords, int nearChunkThreshold) {
        int regionX = chunkCoords.getX() >> 5;
        int regionZ = chunkCoords.getZ() >> 5;

        int chunkXInRegion = chunkCoords.getX() & 31;
        int chunkZInRegion = chunkCoords.getZ() & 31;

        List<String> regions = new ArrayList<>();
        String currentRegion = "r." + regionX + "." + regionZ + ".mca";
        regions.add(currentRegion);

        messageBuilder.append(Text.of(TextColors.GRAY, "Importing the region you are currently in: ", TextColors.DARK_GRAY, currentRegion));

        boolean nearNorth = chunkZInRegion < nearChunkThreshold;
        boolean nearSouth = chunkZInRegion > 31 - nearChunkThreshold;
        boolean nearWest = chunkXInRegion < nearChunkThreshold;
        boolean nearEast = chunkXInRegion > 31 - nearChunkThreshold;

        if (nearWest) {
            addRegionToMessage("West", regionX - 1, regionZ, regions, messageBuilder);
        }
        if (nearEast) {
            addRegionToMessage("East", regionX + 1, regionZ, regions, messageBuilder);
        }
        if (nearNorth) {
            addRegionToMessage("North", regionX, regionZ - 1, regions, messageBuilder);
        }
        if (nearSouth) {
            addRegionToMessage("South", regionX, regionZ + 1, regions, messageBuilder);
        }
        if (nearNorth && nearWest) {
            addRegionToMessage("North-West", regionX - 1, regionZ - 1, regions, messageBuilder);
        }
        if (nearNorth && nearEast) {
            addRegionToMessage("North-East", regionX + 1, regionZ - 1, regions, messageBuilder);
        }
        if (nearSouth && nearWest) {
            addRegionToMessage("South-West", regionX - 1, regionZ + 1, regions, messageBuilder);
        }
        if (nearSouth && nearEast) {
            addRegionToMessage("South-East", regionX + 1, regionZ + 1, regions, messageBuilder);
        }

        messageBuilder.append(Text.of(TextColors.GRAY, "\nImporting ",
                TextColors.DARK_GRAY, regions.size(),
                TextColors.GRAY, " region" + (regions.size() > 1 ? " files" : " file") + " into Lightonia: "));

        for (int i = 0; i < regions.size(); i++) {
            if (i > 0) {
                messageBuilder.append(Text.of(TextColors.GRAY, ", "));
            }
            messageBuilder.append(Text.of(TextColors.DARK_GRAY, regions.get(i)));
        }

        messageBuilder.append(Text.of(TextColors.LIGHT_PURPLE, "\nThis should take a minute or two, but it may take longer than that. Please have patience."));

        return regions;
    }

    private static void addRegionToMessage(String direction, int regionX, int regionZ, List<String> regions, Text.Builder messageBuilder) {
        String regionName = "r." + regionX + "." + regionZ + ".mca";
        regions.add(regionName);
        messageBuilder.append(Text.of(TextColors.GRAY, "\nAnd the nearby region to the ", direction, ": ", TextColors.DARK_GRAY, regionName));
    }

    public static void updateExtentsForImportedRegions(List<String> regionsToImport) {
        for (String regionName : regionsToImport) {
            // Extract region coordinates from the region name
            // Assuming regionName format is "r.x.z.mca"
            String[] parts = regionName.split("\\.");
            int regionX = Integer.parseInt(parts[1]);
            int regionZ = Integer.parseInt(parts[2]);
            WorldFunctions.updateExtents(regionX, regionZ);
        }
    }

    public static void completeImport(CommandSource src, Set<String> regionFiles) {
        String absolutePath = FileFunctions.getBackupPath(src);
        String selectedBackup = absolutePath.substring(absolutePath.lastIndexOf("/") + 1);
        if (!regionFiles.isEmpty()) Lightonia.getLogger().info(src.getName() + " imported regions: " + String.join(", ", regionFiles) + " from " + absolutePath + ".");
        for (String regionFile : regionFiles) {
            importedRegions.put(regionFile, selectedBackup);
        }
        ongoingImports.computeIfPresent(src.getName(), (key, pair) -> {
            if (pair.getLeft().decrementAndGet() <= 0) {
                return null;
            } else {
                pair.getRight().removeAll(regionFiles);
                return pair;
            }
        });
    }

    public static Text getImportingPlayersWithCounts(TextColor baseColour) {
        Text.Builder textBuilder = Text.builder();
        int totalImportingPlayers = (int) ongoingImports.values().stream().filter(pair -> pair.getLeft().get() > 0).count();
        boolean first = true;

        for (Map.Entry<String, Pair<AtomicInteger, List<String>>> entry : ongoingImports.entrySet().stream()
                .filter(e -> e.getValue().getLeft().get() > 0)
                .limit(3)
                .collect(Collectors.toList())) {
            if (!first) {
                textBuilder.append(Text.of(baseColour, ", "));
            }
            first = false;

            int importCount = entry.getValue().getLeft().get();
            String importText = importCount > 1 ? importCount + " imports" : "1 import";

            textBuilder.append(Text.builder()
                    .append(Text.of(TextColors.YELLOW, entry.getKey(), " (", TextColors.RED, importText, TextColors.YELLOW, ")"))
                    .onHover(TextActions.showText(Text.of(TextColors.DARK_GRAY, String.join(", ", entry.getValue().getRight())))).build()
            );
        }

        if (totalImportingPlayers > 3) {
            int additionalPlayers = totalImportingPlayers - 3;
            textBuilder.append(Text.of(TextColors.GRAY, " (and " + additionalPlayers + " more)"));
        }

        return textBuilder.build();
    }

    public static boolean isImporting(CommandSource src) {
        return ongoingImports.containsKey(src.getName());
    }

    public static boolean isLightoniaEmpty() {
        return getRegions().isEmpty();
    }

    public static Map<String, String> getRegions() {
        return importedRegions;
    }

    public static void clearRegions() {
        importedRegions.clear();
    }

    public static CommandCallable getCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "import.use")
                .executor(new Import())
                .build();
    }
}