package com.minecraftonline.lightonia.commands;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.lightonia.Lightonia;

public class Invite extends Teleport {

    private static final Text PLAYER = Text.of("player");

    private final boolean accepting;

    public Invite(boolean accept) {
        super(TeleportType.Lightonia);
        accepting = accept;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("You must be a player to use this command."));
        }
        Player player = (Player) src;
        if (accepting) {
            acceptInvitation(player, args.getOne(PLAYER));
        } else { // inviting
            inviteToLightonia(player, args.requireOne(PLAYER));
        }
        return CommandResult.success();
    }

    // map of invitee UUID to inviter UUID(s).
    private static final HashMap<UUID, HashSet<UUID>> invites = new HashMap<UUID, HashSet<UUID>>();

    private void inviteToLightonia(Player player, Player invitee) {
        if (!player.getWorld().getName().equals("Lightonia")) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "You must be in Lightonia to use this command."));
            return;
        }
        if (player.equals(invitee)) {
            player.sendMessage(Text.of(TextColors.DARK_RED, "You can't invite yourself to Lightonia!"));
            return;
        }
        if (!invitee.isOnline()) {
            player.sendMessage(Text.of(TextColors.DARK_RED, invitee.getName(), " is not currently online."));
            return;
        }
        if (invitee.getWorld().getName().equals("Lightonia")) {
            player.sendMessage(Text.of(TextColors.DARK_RED, invitee.getName(), " is already in Lightonia."));
            return;
        }
        if (!invites.containsKey(invitee.getUniqueId())) {
            invites.put(invitee.getUniqueId(), new HashSet<UUID>());
        }
        if (invites.get(invitee.getUniqueId()).add(player.getUniqueId())) {
            player.sendMessage(Text.of(TextColors.GREEN, invitee.getName(), " has been sent an invite to Lightonia."));
            invitee.sendMessage(Text.of(TextColors.GREEN, player.getName(), " has invited you to Lightonia, use ",
                                TextColors.RED, "/Lightonia accept ", player.getName(),
                                TextColors.GREEN, " to accept it"));
        } else { // already invited
            player.sendMessage(Text.of(TextColors.DARK_RED, invitee.getName(), " had already been sent an invite to Lightonia from you."));
        }
    }

    private void acceptInvitation(Player invitee, Optional<Player> player) {
        if (invitee.getWorld().getName().equals("Lightonia")) {
            invitee.sendMessage(Text.of(TextColors.DARK_RED, "You are already in Lightonia."));
            return;
        }
        if (!invites.containsKey(invitee.getUniqueId())) {
            invitee.sendMessage(Text.of(TextColors.DARK_RED, "Nobody has invited you to Lightonia recently."));
            return;
        }

        if (player.isPresent()) {
            if (invites.get(invitee.getUniqueId()).contains(player.get().getUniqueId())) {
                if (playerIsInLightonia(player.get())) {
                    if (!lightoniaTeleport(invitee, invitee.getPosition(), player.get().getPosition(), false)) return;
                    Lightonia.getLogger().info(invitee.getName() + " teleported to " + player.get().getName() + " in Lightonia.");
                    player.get().sendMessage(Text.of(TextColors.GREEN, invitee.getName(), " has accepted your invitation to Lightonia."));
                    invitee.sendMessage(Text.of(TextColors.GREEN, "You accepted an invite to Lightonia from ", player.get().getName()));
                    invites.remove(invitee.getUniqueId());
                } else {
                    invitee.sendMessage(Text.of(TextColors.DARK_RED, player.get().getName(), " is not currently in Lightonia."));
                }
            } else {
                invitee.sendMessage(Text.of(TextColors.DARK_RED, player.get().getName(), " has not sent you an invite to Lightonia recently."));
            }
            return;
        }

        for (UUID uuid : invites.get(invitee.getUniqueId()))
        {
            Optional<Player> inviter = Sponge.getServer().getPlayer(uuid);
            if (inviter.isPresent() && playerIsInLightonia(inviter.get())) {
                if (!lightoniaTeleport(invitee, invitee.getPosition(), inviter.get().getPosition(), false)) return;
                Lightonia.getLogger().info(invitee.getName() + " teleported to " + inviter.get().getName() + " in Lightonia.");
                inviter.get().sendMessage(Text.of(TextColors.GREEN, invitee.getName(), " has accepted your invitation to Lightonia."));
                invitee.sendMessage(Text.of(TextColors.GREEN, "You accepted an invite to Lightonia from ", inviter.get().getName()));
                invites.remove(invitee.getUniqueId());
                return;
            }
        }
        invitee.sendMessage(Text.of(TextColors.DARK_RED, "No players currently in Lightonia have invited you recently."));
    }

    private boolean playerIsInLightonia(Player player) {
        return player.isOnline() && player.getWorld().getName().equals("Lightonia");
    }

    public static CommandCallable getInviteCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "invite.use")
                .executor(new Invite(false))
                .arguments(GenericArguments.onlyOne(GenericArguments.player(PLAYER)))
                .build();
    }

    public static CommandCallable getAcceptCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "accept.use")
                .executor(new Invite(true))
                .arguments(GenericArguments.optional(GenericArguments.onlyOne(GenericArguments.player(PLAYER))))
                .build();
    }

    public static void clearInvites() {
        invites.clear();
    }

}
