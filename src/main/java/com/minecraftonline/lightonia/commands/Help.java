package com.minecraftonline.lightonia.commands;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.lightonia.Lightonia;

public class Help implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) {
        src.sendMessage(Text.of(
            TextColors.GOLD, "*** ", TextColors.LIGHT_PURPLE, "Lightonia Commands", TextColors.GOLD, " ***",
            TextColors.LIGHT_PURPLE, "\n/Lightonia list <type>: ",
            TextColors.WHITE, "Lists available backups. Type can be either 'World' or 'Player'.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia select <type> <backup>: ",
            TextColors.WHITE, "Select which backup to use. Type can be either 'World' or 'Player'.\"",
            TextColors.LIGHT_PURPLE, "\n/Lightonia invsee <player>: ",
            TextColors.WHITE, "Opens up the player's inventory from the selected player backup. This includes their armour and their off-hand slot.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia ecsee <player>: ",
            TextColors.WHITE, "Opens up the player's enderchest from the selected player backup.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia import: ",
            TextColors.WHITE, "The backup region file(s) corresponding to the one you are standing in gets imported into Lightonia.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia importfrom <x> <y> <z> <world>: ",
            TextColors.WHITE, "The backup region file(s) corresponding the coordinates and world gets imported into Lightonia.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia query: ",
            TextColors.WHITE, "Checks if there are any regions currently being imported into Lightonia and gives status of Lightonia.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia create: ",
            TextColors.WHITE, "Creates and loads the Lightonia world.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia tp: ",
            TextColors.WHITE, "Teleports you to Lightonia.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia return: ",
            TextColors.WHITE, "Teleports you back to your original position.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia switch: ",
            TextColors.WHITE, "Teleports you to the same position in the previous world.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia invite <player>: ",
            TextColors.WHITE, "Invites another player to teleport to you to Lightonia.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia accept [player]: ",
            TextColors.WHITE, "Teleports you to a player in Lightonia that has invited you.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia reload: ",
            TextColors.WHITE, "Reloads the Lightonia configuration file.",
            TextColors.LIGHT_PURPLE, "\n/Lightonia clear: ",
            TextColors.WHITE, "The Lightonia world is unloaded and its region files are deleted."
        ));

        return CommandResult.success();
    }

    public static CommandCallable getCommand() {
        return CommandSpec.builder()
                .permission(Lightonia.BASE_PERMISSION + "help.use")
                .executor(new Help())
                .build();
    }
}