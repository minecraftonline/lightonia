package com.minecraftonline.lightonia;

import com.flowpowered.math.vector.Vector3d;
import com.google.inject.Inject;
import com.minecraftonline.lightonia.commands.CommandHub;
import com.minecraftonline.lightonia.util.WorldFunctions;

import java.util.Optional;

import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.world.World;

@Plugin(id = "lightonia",
        name = "Lightonia",
        description = "Allow players to access backups in-game",
        authors = {"AlphaAlex115, Anna_28, doublehelix457, tyhdefu"})
public class Lightonia {

    private static Lightonia plugin;
    private static Logger logger;

    public static final String BASE_PERMISSION = "lightonia.";

    @Inject
    public Lightonia(Logger logger) {
        plugin = this;
        Lightonia.logger = logger;
    }

    public static Lightonia getPlugin() {
        return plugin;
    }

    public static Logger getLogger() {
        return logger;
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        ConfigFile.loadConfig();
        WorldFunctions.unloadThenClearLightonia(Sponge.getServer().getConsole());
    }

    @Listener
    public void pluginReload(GameReloadEvent event) {
        ConfigFile.loadConfig();
    }

    @Listener
    public void init(GameInitializationEvent event) {
        Sponge.getCommandManager().register(this, CommandHub.getRootCommand(), "lightonia");
    }

    @Listener
    public void onJoin(ClientConnectionEvent.Join event) {
        // it would be better to set the player position on login or in SpawnEntityEvent maybe?
        // rather than teleporting them directly after logging in like this.
        Player player = event.getTargetEntity();
        Vector3d pos = player.getPosition();
        if (!player.getWorld().getName().equals("Lightonia")) return;
        // don't let players login to areas of Lightonia that are not inside the border
        if (!WorldFunctions.isInsideBorderForLightonia(pos.getX(), pos.getZ())) {
            Lightonia.logger.info("Player " + player.getName() + " is joining outside the border of Lightonia");
            Optional<World> world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName());
            if (world.isPresent()) {
                Lightonia.logger.info("Sending player " + player.getName() + " to default world");
                // send them to the default world spawn instead of Lightonia void
                player.setLocation(world.get().getSpawnLocation());
            } else {
                Lightonia.getLogger().warn("Could not find the default world to teleport " + player.getName() + " to from Lightonia on join.");
            }
        }
    }
}