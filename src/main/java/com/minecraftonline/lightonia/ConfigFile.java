package com.minecraftonline.lightonia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;

public class ConfigFile {

    private static Logger logger = Lightonia.getLogger();
    public static String worldPath = "";
    public static List<String> worldBackupPaths = new ArrayList<>();
    public static List<String> playerBackupPaths = new ArrayList<>();
    public static String getUUIDFromMojangAPI = "";
    public static String dbUsername = "";
    public static String dbPassword = "";
    public static String dbName = "";
    public static String dbAddress = "";
    public static String dbQuery = "";
    public static String dummyUUID0 = "";
    public static String dummyUUID1 = "";
    public static String dummyUUID2 = "";
    public static String dummyUUID3 = "";
    public static String dummyUUID4 = "";
    public static String dummyUUID5 = "";
    public static String dummyUUID6 = "";
    public static String dummyUUID7 = "";

    public static void loadConfig() {
        File configFile = new File("config/lightonia/lightonia.conf");

        if (!configFile.exists()) {
            createDefaultConfig(configFile);
        }

        Properties properties = new Properties();
        try (FileInputStream inputStream = new FileInputStream(configFile)) {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.error("Failed to load the Lightonia configuration file", e);
            // Delete the existing config file
            if (configFile.exists()) {
                configFile.delete();
            }
            createDefaultConfig(configFile);
            try (FileInputStream inputStream = new FileInputStream(configFile)) {
                properties.load(inputStream);
            } catch (IOException ex) {
                logger.error("Failed to load the Lightonia configuration file after recreating it", ex);
                return;
            }
        }

        worldPath = properties.getProperty("worldPath", "/home");
        worldBackupPaths = Arrays.asList(properties.getProperty("worldBackupPaths", "/home").split("\\s*,\\s*"));
        playerBackupPaths = Arrays.asList(properties.getProperty("playerBackupPaths", "/home").split("\\s*,\\s*"));
        getUUIDFromMojangAPI = properties.getProperty("getUUIDFromMojangAPI", "true");
        dbUsername = properties.getProperty("dbUsername", "user");
        dbPassword = properties.getProperty("dbPassword", "pass");
        dbName = properties.getProperty("dbName", "luckperms");
        dbAddress = properties.getProperty("dbAddress", "localhost");
        dbQuery = properties.getProperty("dbQuery", "SELECT uuid FROM players WHERE username = ? AND uuid IS NOT NULL ORDER BY uuid DESC LIMIT 1");
        dummyUUID0 = properties.getProperty("dummyUUID0", "069a79f4-44e9-4726-a5be-fca90e38aaf5");
        dummyUUID1 = properties.getProperty("dummyUUID1", "853c80ef-3c37-49fd-aa49-938b674adae6");
        dummyUUID2 = properties.getProperty("dummyUUID2", "c7f0141d-f239-4ff3-8130-10cd0462c7a6");
        dummyUUID3 = properties.getProperty("dummyUUID3", "b9583ca4-3e64-488a-9c8c-4ab27e482255");
        dummyUUID4 = properties.getProperty("dummyUUID4", "0c5a016f-236c-4785-98ea-c5092a042b5f");
        dummyUUID5 = properties.getProperty("dummyUUID5", "9c2ac958-5de9-45a8-8ca1-4122eb4c0b9e");
        dummyUUID6 = properties.getProperty("dummyUUID6", "50c14e90-7478-44bf-adee-55fb0196efd5");
        dummyUUID7 = properties.getProperty("dummyUUID7", "bacf0860-c190-4647-9d90-64dd18df8c45");
    }

    private static void createDefaultConfig(File configFile) {
        configFile.getParentFile().mkdirs();
        Properties defaultProperties = new Properties();
        defaultProperties.setProperty("worldPath", "/home");
        defaultProperties.setProperty("worldBackupPaths", "/home");
        defaultProperties.setProperty("playerBackupPaths", "/home");
        defaultProperties.setProperty("getUUIDFromMojangAPI", "true");
        defaultProperties.setProperty("dbUsername", "user");
        defaultProperties.setProperty("dbPassword", "pass");
        defaultProperties.setProperty("dbName", "luckperms");
        defaultProperties.setProperty("dbAddress", "localhost");
        defaultProperties.setProperty("dbQuery", "SELECT uuid FROM players WHERE username = ? AND uuid IS NOT NULL ORDER BY uuid DESC LIMIT 1");
        defaultProperties.setProperty("dummyUUID0", "069a79f4-44e9-4726-a5be-fca90e38aaf5");
        defaultProperties.setProperty("dummyUUID1", "853c80ef-3c37-49fd-aa49-938b674adae6");
        defaultProperties.setProperty("dummyUUID2", "c7f0141d-f239-4ff3-8130-10cd0462c7a6");
        defaultProperties.setProperty("dummyUUID3", "b9583ca4-3e64-488a-9c8c-4ab27e482255");
        defaultProperties.setProperty("dummyUUID4", "0c5a016f-236c-4785-98ea-c5092a042b5f");
        defaultProperties.setProperty("dummyUUID5", "9c2ac958-5de9-45a8-8ca1-4122eb4c0b9e");
        defaultProperties.setProperty("dummyUUID6", "50c14e90-7478-44bf-adee-55fb0196efd5");
        defaultProperties.setProperty("dummyUUID7", "bacf0860-c190-4647-9d90-64dd18df8c45");

        try (FileOutputStream outputStream = new FileOutputStream(configFile)) {
            defaultProperties.store(outputStream, "Configuration for the Lightonia plugin.\nNote 1: if getUUIDFromMojangAPI is set to true, do not worry about the database configuration."
                    + "\nNote 2: use forward slashes for absolute paths. For example, on Windows, the worldPath should look like this: C:/Users/sam/Desktop/test_server/world"
                    + "\nNote 3: for worldBackupPaths and playerBackupPaths, you can separate different backup paths by commas:"
                    + "\nworldBackupPaths=/path/to/backup1,/path/to/backup2,/path/to/backup3");
        } catch (IOException e) {
            logger.error("Failed to create the default Lightonia configuration file.", e);
        }
    }
}