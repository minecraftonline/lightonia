# Lightonia: Access Backups In-game

### Features

* Select which backups you wish to use while in game.
* Access backup regions.
* Access backup player inventories. This includes their armour and their off-hand slot.

### Instructions

Once you run the server with the Lightonia.jar file, a configuration file will be created in the /config folder. Edit the configuration file to include the absolute paths to your backup directories as well as your world directory, and ensure you only include forward slashes (/). While in-game you can then do /Lightonia reload and run the commands found below.

### Commands

#### /Lightonia list &lt;type&gt;
Lists available backups. Type can be either 'World' or 'Player'.

#### /Lightonia select &lt;type&gt; &lt;backup&gt;
Select which backup to use. Type can be either 'World' or 'Player'.

#### /Lightonia invsee &lt;player&gt;
Opens up the player's inventory from the selected player backup. This includes their armor and their off-hand slot.

#### /Lightonia ecsee &lt;player&gt;
Opens up the player's enderchest from the selected player backup.

#### /Lightonia import
The backup region file corresponding to the one you are standing in gets imported into Lightonia.

#### /Lightonia create
Creates and loads the Lightonia world.

#### /Lightonia tp
Teleports you to Lightonia.

#### /Lightonia tpb
Teleports you back to your original position.

#### /Lightonia reload
Reloads the Lightonia configuration file.

#### /Lightonia destroy
The Lightonia world is unloaded and its files are deleted.

### Support Me
Donations to my [patreon](https://www.patreon.com/lolwhatyesme) will help with the development of this plugin.
